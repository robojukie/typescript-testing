var emitter = require('./events_service').emitter;

var blacklistService = require('./blacklist_service');

exports.setPhone = function(phone) {
    if (!/^\d+$/.test(phone) || blacklistService.isInBlackList(phone)) {
        emitter.emit('invalidPhone', phone);
    }
}