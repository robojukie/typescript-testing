export class Spy {
  // A func that takes in two parameters `req` and `res` [request, response]
  getSpyPage(req, res){
    return res.send("Hey");
  }
}