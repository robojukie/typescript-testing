// import assert = require('assert');
// import { Box } from '../src';
// import 'mocha';
// import { expect } from 'chai';
// import chai = require('chai');
// import sinon from 'sinon';

// chai.use(require('sinon-chai'));
// chai.use(require('chai-as-promised'));
// // import our getIndexPage function
// // import indexPage from "../src/Controller.js";
// import { IndexPage } from '../src';

// describe("smoke test", function() {
//   it("checks equality", function() {
//     assert.equal(true, true);
//   });
// });

// describe('Testing Box', function() {
//   it('should assert the volume of the Box', function() {
//     var obj = new Box(10, 20, 30);
//     assert.equal(obj.getVolume(), 6000);
//   })
// })

// describe("smoke test", function() {
//   it("checks equality", function() {
//     assert.equal(true, true);
//   });
// });

// describe("smoke test", function() {
//   it("checks equality", function() {
//     expect(true).to.be.true;
//   });
// });

// describe("AppController", function()  {
//   describe("getIndexPage", function() {
//     it("should send hey when user is logged in", function() {
//       let user = {
//         isLoggedIn: function(): any {}
//       }

//       // Stub isLoggedIn function and make it return true always
//       const isLoggedInStub = sinon.stub(user, "isLoggedIn").returns(true);

//       let req = {
//         user: user
//       }

//       // Have `res` have a send key with a function value coz we use `res.send()` in our func
//       let res = {
//         // replace empty function with a spy
//         // send: sinon.spy()

//         send: function(){}
//       }

//       // mock res
//       const mock = sinon.mock(res);
//       // build how we expect it t work
//       mock.expects("send").once().withExactArgs("Hey");
      
//       let indexPage = new IndexPage();
//       indexPage.getIndexPage(req, res);
//       // // let's see what we get on res.send
//       // // console.log(res.send);
//       // // `res.send` called once
//       // expect(res.send.calledOnce).to.be.true;
//       // expect(res.send.firstCall.args[0]).to.equal("Hey");
      
//       // // assert that the stub is logged in at least once
//       expect(isLoggedInStub.calledOnce).to.be.true;
//       mock.verify();
//     });

//     it("should send something else when user is NOT logged in", function() {
//       // instantiate a user object with an empty isLoggedIn function
//       let user = {
//         isLoggedIn: function(): any {}
//       }

//       // Stub isLoggedIn function and make it return false always
//       const isLoggedInStub = sinon.stub(user, "isLoggedIn").returns(false);

//       // pass user into the req object
//       let req = {
//         user: user
//       }

//       // Have `res` have a send key with a function value coz we use `res.send()` in our func
//       let res = {
//         // replace empty function with a spy
//         send: sinon.spy()
//       }
//       let indexPage = new IndexPage();
//       indexPage.getIndexPage(req, res);
//       // let's see what we get on res.send
//       // console.log(res.send);
//       // `res.send` called once
//       expect(res.send.calledOnce).to.be.true;
//       expect(res.send.firstCall.args[0]).to.equal("Ooops. You need to log in to access this page");

//       // assert that the stub is logged in at least once
//       expect(isLoggedInStub.calledOnce).to.be.true;
//     })
//   });
// });



// This is just an async func that takes in a bool
// and calls a callback that returns a some message
// depending on the bool value
// function someMadeUpAyncFunc(boolValue, cb) {
  // setTimeout(function() {
  //   cb(boolValue ? "You get a sweet :)" : "You get nothing!!")
  // }, 0);
// }

// This is just an async func that takes in a bool
// and that returns a promise
// function someMadeUpAyncFunc(boolValue, cb) {
//   return new Promise(function(resolve){
//     setTimeout(function() {
//       resolve(boolValue ? "You get a sweet :)" : "You get nothing!!")
//     }, 0);
//   })
// }

// // Added the `only` tag to have only this set of tests to run
// describe("AsyncTest", function()  {
//   it("should return `You get a sweet :)` if `true` is passed in", function() {
//     return expect(someMadeUpAyncFunc(true)).to.eventually.equal("You get a sweet :)");
//     // someMadeUpAyncFunc(true, function(sweetCheck){
//     //   expect(sweetCheck).to.equal("You get a sweet :)");
//     //   done();
//     // });
//   });

//   it("should return `You get nothing!!` if `false` is passed in", function() {
//     return expect(someMadeUpAyncFunc(false)).to.eventually.equal("You get nothing!!");
//     // someMadeUpAyncFunc(false, function(sweetCheck){
//     //   // Let's fail it on purpose just to see what happens
//     //   expect(sweetCheck).to.equal("You get a sweet :)");
//     //   done();
//     // });
//   });
// });
