// External calls which make tests slow and difficult to write (e.g HTTP calls/ DB calls)
// Triggering different outcomes for a piece of code (e.g. what happens if an error is thrown/ if it passes)

import chai = require('chai');
const expect = chai.expect;
import sinon from 'sinon';
import { stubObject } from 'ts-sinon';
// const stubObject = tssinon.stubObject;

// import our getIndexPage function
import { IndexPage } from '../../src';

class Test {
  method() { return 'original' }
}

const test = new Test();
const testStub = stubObject<Test>(test) as any;

describe('test stub ', () => {
  it('should returned new string', () => {
    testStub.method.returns('stubbed');
    expect(testStub.method()).to.equal('stubbed');
  })
})


describe("AppController", function()  {
  describe("getIndexPage stub example", function() {
    it("should send hey when user is logged in", function() {
      let user = {
        isLoggedIn: function(): any {}
      }

      // Stub isLoggedIn function and make it return true always
      const isLoggedInStub = sinon.stub(user, "isLoggedIn").returns(true);

      // pass user into the req object
      let req = {
        user: user
      }

      // Have `res` have a send key with a function value coz we use `res.send()` in our func
      let res = {
        // replace empty function with a spy
        send: sinon.spy()
      }
      
      let indexPage = new IndexPage();
      indexPage.getIndexPage(req, res);
      // // let's see what we get on res.send
      // console.log(res.send);
      expect(isLoggedInStub.calledOnce).to.be.true;
    });

    it("should send something else when user is NOT logged in", function() {
      // instantiate a user object with an empty isLoggedIn function
      let user = {
        isLoggedIn: function(): any {}
      }

      // Stub isLoggedIn function and make it return false always
      const isLoggedInStub = sinon.stub(user, "isLoggedIn").returns(false);

      // pass user into the req object
      let req = {
        user: user
      }

      // Have `res` have a send key with a function value coz we use `res.send()` in our func
      let res = {
        // replace empty function with a spy
        send: sinon.spy()
      }

      let indexPage = new IndexPage();
      indexPage.getIndexPage(req, res);

      // `res.send` called once
      expect(res.send.calledOnce).to.be.true;
      expect(res.send.firstCall.args[0]).to.equal("Ooops. You need to log in to access this page");

      // assert that the stub is logged in at least once
      expect(isLoggedInStub.calledOnce).to.be.true;
    })
  });
});
