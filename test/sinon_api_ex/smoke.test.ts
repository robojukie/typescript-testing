/**
 * async and mock
 */
import { expect } from 'chai';
import chai = require('chai');
import sinon = require('sinon');
import { IndexPage } from '../../src';

chai.use(require('chai-as-promised'));

// This is just an async func that takes in a bool
// and calls a callback that returns a some message
// depending on the bool value
// function someMadeUpAyncFunc(boolValue, cb) {
  // setTimeout(function() {
  //   cb(boolValue ? "You get a sweet :)" : "You get nothing!!")
  // }, 0);
// }



/**
 * Start async/await example
 * not a test - trying out conversion to async feature of vscode
 * 
 * Promise chain code snippet:
 // function example() {
 //   return Promise.resolve(1)
 //     .then(() => {
 //       return Promise.resolve(2);
 //     }).then((value) => {
 //       console.log(value)
 //       return Promise.reject(3)
 //     }).catch(err => {
 //       console.log(err);
 //     })
 // }
 */
// converted async/await
async function example() {
  try {
    await Promise.resolve(1);
    const value = await Promise.resolve(2);
    console.log(value);
    return Promise.reject(3);
  }
  catch (err) {
    console.log(err);
  }
}
/** End of async/await example */

// function get() {
//   return fetch('https://umaar.com')
//     .then(res)
// }

// This is just an async func that takes in a bool
// and that returns a promise
function someMadeUpAsyncFunc(boolValue, cb) {
  return new Promise(function(resolve){
    setTimeout(function() {
      resolve(boolValue ? "You get a sweet :)" : "You get nothing!!")
    }, 0);
  })
}

// Added the `only` tag to have only this set of tests to run
describe("AsyncTest", function()  {
  it('should throw err', function() {
    var badFn = function () { throw new TypeError('Illegal salmon!'); };

    expect(badFn).to.throw(TypeError);
  })
  
  it("should return `You get a sweet :)` if `true` is passed in", function() {
    return expect(someMadeUpAsyncFunc(true, someMadeUpAsyncFunc)).to.eventually.equal("You get a sweet :)");
  });

  it("should return `You get nothing!!` if `false` is passed in", function() {
    return expect(someMadeUpAsyncFunc(false, someMadeUpAsyncFunc)).to.eventually.equal("You get nothing!!");
  });
});


/**
 * With mocks we can specify how we want something to work and use mock.verify() to make sure it works. This means less and cleaner code. With our already existing tests we could:
 * mock our res object
 * expect send to be called once with the argument Hey
 * then call mock.verify()
 */

describe("AppController", function()  {
  describe("getIndexPage mock example", function() {
    it("should send hey when user is logged in", function() {
      // instantiate a user object with an empty isLoggedIn function
      let user = {
        isLoggedIn: function(): any {}
      }

      // Stub isLoggedIn function and make it return true always
      const isLoggedInStub = sinon.stub(user, "isLoggedIn").returns(true);

      // pass user into the req object
      let req = {
        user: user
      }

      // Have `res` have a send key with a function value coz we use `res.send()` in our func
      let res = {
        send: function(){}
      }

      // mock res
      const mock = sinon.mock(res);
      // build how we expect it t work
      mock.expects("send").once().withExactArgs("Hey");

      let indexPage = new IndexPage();
      indexPage.getIndexPage(req, res);
      expect(isLoggedInStub.calledOnce).to.be.true;

      // verify that mock works as expected
      mock.verify();
    });
  });
});



// function once(fn: any) {
//   var returnValue: any, called = false;
//   return function () {
//       if (!called) {
//           called = true;
//           returnValue = fn.apply(fn, arguments);
//         }
//       return returnValue;
//   };
// }
// describe('test stub ', () => {
//   it('callback called', function () {
//     var callback = sinon.fake();
//     var proxy = once(callback);

//     var callback = sinon.fake.returns(42);
//     var proxy = once(callback);

//     assert.equal(proxy(), 42);
//     proxy();
//     proxy();

//     // assert(callback.called);
//     assert(callback.calledOnce);
//     // ...or:
//     // assert.equals(callback.callCount, 1);
//     var obj = {};

//     var obj2 = [1,2,3];

//     proxy.call(obj2);

//     assert(callback.calledOn(obj2));
//     assert(callback.calledWith(1, 2, 3));
//   });
// })