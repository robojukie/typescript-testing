import chai = require('chai');
import { expect } from 'chai';
import sinon from 'sinon';
import { Spy } from '../../src';
import 'mocha';
// import * as tssinon from "ts-sinon";
chai.use(require('sinon-chai'));

describe("AppController", function()  {
  describe("spy example", function() {
    it("should send hey ex 2", function() {
      let req = {}

      // Have `res` have a send key with a function value coz we use `res.send()` in our func
      let res = {
        // replace empty function with a spy
        send: sinon.spy()
      }
      let spyPage = new Spy();
      spyPage.getSpyPage(req, res)

      // `res.send` called once
      expect(res.send.calledOnce).to.be.true;
      expect(res.send.firstCall.args[0]).to.equal("Hey");
    });
  });
});

class user {
  private name: string = '';
  public addUser(name: string): any {
    this.name = name;
  }
}

describe("User", function() {
  describe("addUser spy example", function() {
    it("should add a user", function() {
      let u = new user();
      sinon.spy(u, "addUser");
      u.addUser('John Doe');

      // lets log `addUser` and see what we get
      // console.log(u.addUser);
      expect(u.addUser).to.have.been.calledOnce;
    });
  });
});