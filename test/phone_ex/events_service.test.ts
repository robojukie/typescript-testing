import sinon from 'sinon';
import { should, expect } from 'chai';

var eventService = require('../../src/phone_ex/events_service');

var emitter = eventService.emitter;

describe('Event service', function() {

    beforeEach( function() {
        this.consoleSpy = sinon.spy(console, 'log');
    });

    afterEach( function() {;
        this.consoleSpy.restore();
    });

    it('Emitted errors should be logged', function() {
        emitter.emit('invalidPhone', 'a123456789');
        // this.consoleSpy.called.should.be.true;
      expect(this.consoleSpy.calledOnce).to.be.true;
    });

});