import { should, expect } from 'chai';
import sinon from 'sinon';

var phoneService = require('../../src/phone_ex/phone_service');
var eventService = require('../../src/phone_ex/events_service');
var blacklistService = require('../../src/phone_ex/blacklist_service');

var emitter = eventService.emitter;

describe.only('Phone service mock', function(){

    beforeEach( function() {
        this.emitSpy = sinon.spy(emitter, 'emit');
        this.blacklistServiceMock = sinon.mock(blacklistService);
    });

    afterEach( function() {
        this.emitSpy.restore();
        this.blacklistServiceMock.restore();
    });

    it('valid phones not in blacklist should not emit an error', function() {
        this.blacklistServiceMock.expects('isInBlackList').withExactArgs('123456789').returns(false);
        phoneService.setPhone('123456789');
        this.blacklistServiceMock.verify();
        // this.emitSpy.called.should.be.false;
        expect(this.emitSpy.called).to.be.false;
    });

    it('valid phones in blacklist should emit an error', function() {
        this.blacklistServiceMock.expects('isInBlackList').withExactArgs('123456789').returns(true);
        phoneService.setPhone('123456789');
        this.blacklistServiceMock.verify();
        expect(this.emitSpy.calledWith('invalidPhone', '123456789')).to.be.true;
    });

    it('invalid phones should emit an error', function() {
        phoneService.setPhone('a123456789');
        // this.emitSpy.calledWith('invalidPhone', 'a123456789').should.be.true;
        expect(this.emitSpy.calledWith('invalidPhone', 'a123456789')).to.be.true;
    });
});