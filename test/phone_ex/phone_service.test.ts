import { should, expect } from 'chai';
import sinon from 'sinon';

var phoneService = require('../../src/phone_ex/phone_service');
var eventService = require('../../src/phone_ex/events_service');
var blacklistService = require('../../src/phone_ex/blacklist_service');

var emitter = eventService.emitter;

describe('Phone service module', function(){

    beforeEach( function() {
        this.emitSpy = sinon.spy(emitter, 'emit');
        this.isInBlackListStub = sinon.stub(blacklistService, 'isInBlackList');
    });

    afterEach( function() {
        this.emitSpy.restore();
        this.isInBlackListStub.restore();
    });

    it('valid phones not in blacklist should not emit an error', function() {
        this.isInBlackListStub.returns(false);
        phoneService.setPhone('123456789');
        expect(this.emitSpy.called).to.be.false;
    })

    it('valid phones in blacklist should emit an error', function() {
        this.isInBlackListStub.returns(true);
        phoneService.setPhone('123456789');
        expect(this.emitSpy.calledWith('invalidPhone', '123456789')).to.be.true;
    });

    it('valid phones should not emit an error', function() {
        phoneService.setPhone('123456789');
        expect(this.emitSpy.called).to.be.false;

    });

    it('invalid phones should emit an error', function() {
        phoneService.setPhone('a123456789');
        expect(this.emitSpy.calledWith('invalidPhone', 'a123456789')).to.be.true;
    });

});