This repo contains codebits from various sinonjs tutorials converted to typescript

spy, stub, Controller  
https://scotch.io/tutorials/how-to-test-nodejs-apps-using-mocha-chai-and-sinonjs

events_service, phone_service  
https://solidgeargroup.com/unit-tests-javascript-sinon
- todo: look into testing private methods (rewire)

Stack  
https://medium.com/@FizzyInTheHall/run-typescript-mocha-tests-in-visual-studio-code-58e62a173575

unit testing reference:  
https://www.richardkotze.com/coding/promises-async-await-testing

useful info:
https://github.com/chaijs/chai/issues/430

---
`npm test`  
to run tests

---
todo:
add example for async to.throw() 

export class SomeClass extends Error {
  async someMethod(foo: string, bar: boolean): Promise<any> {
    if (this.methodInSomeMethod) {
      const err = new Error(`err message`);
      throw err;
    }
    ... blah blah some call await anotherMethod
  }
}

it('should throw error', async function() {
  let someService = new SomeClass(); 
  let call = someService.someMethod(string, bool);
  
  expect(this.someStubForMethodInSomeMethod.called).to.be.true;
  expect(call).to.eventually.throw().and.equal(`err message`);
});
